use strict;
use utf8;
use Win32::OLE;
use Win32::OLE::Const 'Microsoft Word';

my $filename = "c:\\work_dir\\perl\\wordtest\\test.doc";
convert_doc();

sub convert_doc{
  my $Word = Win32::OLE->GetActiveObject('Word.Application') || Win32::OLE->new('Word.Application');
  $Word->{'Visible'}     = 0;
  $Word->{DisplayAlerts} = 0;

   # Open File
  my $File = $Word->Documents->Open($filename);

  # フィールドコレクションの取得
  my $field_list = $File->Fields();

  # フィールドコレクションのカウント
  my $field_count = $File->Fields->Count;

  for (my $i = 1; $i <= $field_count; $i++){
    # フィールドコレクションからフィールドを取得するには->Item()を使用する
    my $field = $field_list->Item($i);

    # こっからは普通にフィールドから取れる
    # VBAオブジェクトにアクセスする場合、最後を{}で囲う。
    if($field->{Type} == wdFieldIncludePicture) {
      if($field->LinkFormat->{Type} == wdLinkTypePicture){
        $field->LinkFormat->{SavePictureWithDocument} = 1;
      }
    }
  }
  # Save As
  $Word->ActiveDocument->SaveAs({
                FileName   => $filename,
                FileFormat => wdFormatDocument});
  # Quit/Close
  $File->Close(); 
  $Word->Quit();
}
